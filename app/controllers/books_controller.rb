class BooksController < ApplicationController
  before_action :authenticate
  def index
    @books = Book.all

  end

  def create
    max_time
    return if performed?
    min_time
    return if performed?

    @user_id = session[:user_id]
    @book = Book.new(book_params)
    if @book.save
      render :success
    end
  end

  private

  def book_params
    params.require(:book).permit(:user_id, :doctor_id)
  end

  def max_time
    count_book = Book.where(:doctor_id => params[:doctor_id]).count(:id)
    if count_book >= 10
      render :action_max_book and return
    end
  end

  def min_time
    @schedules = Schedule.where(:doctor_id => params[:doctor_id])
    @schedules.each do |s|
      start_hour = DateTime.parse("2019-08-15 " + s.hour.slice(0..4))
      end_hour = DateTime.parse("2019-08-15 " + s.hour.slice(8..12))

      schedule = Time.now - 30 * 60
      day = Time.now.strftime("%A").downcase
      hour = schedule.hour
      minute = schedule.min
      if hour > start_hour.hour
        render :action_min_time and return
      end

      if hour == start_hour.hour
        if minute >= start_hour.min
          render :action_min_time and return
        end
      end

    end

  end
end