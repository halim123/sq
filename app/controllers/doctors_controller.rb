class DoctorsController < ApplicationController
  before_action :authenticate
  def index
    @hospitals = Hospital.all
  end
end
