class Doctor < ApplicationRecord
  belongs_to :hospital
  has_many :books
  has_many :schedules
end
