json.response @books do |book|
  json.booking_id  book.id
  json.user_data book.user
  json.doctor_data book.doctor

end