json.response @hospitals do |hospital|
  json.hospital_name hospital.name

  json.doctors hospital.doctors do |doctor|
    json.doctor_id doctor.id
    json.name doctor.name
    json.specialization doctor.specialization

    json.schedules doctor.schedules do |schedule|
      json.day schedule.day
      json.hour schedule.hour
    end
  end

end