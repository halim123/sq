class CreateDoctors < ActiveRecord::Migration[5.2]
  def change
    create_table :doctors do |t|
      t.integer :hospital_id
      t.string :name
      t.string :specialization
      t.references :hospital, foreign_key: true

      t.timestamps
    end
  end
end
